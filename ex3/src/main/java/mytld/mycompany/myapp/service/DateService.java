package mytld.mycompany.myapp.service;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//@Service
//@Component
public class DateService {

	private static final Logger logger = LoggerFactory.getLogger(DateService.class);
	
	public DateService(int i, String s) {
//		super();
		logger.info(String.format("parametry: %d, %s", i, s));
	}
	
	public String getCurrentDate(Locale locale) {
		
		logger.info("inside getCurrentDate");
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		return formattedDate;
	}


//	@Autowired
//	public void wyszukaj(UzytkownikDao uzytkownikDao, AdminDao adminDao) {
//		// cos
//	}
	
}
