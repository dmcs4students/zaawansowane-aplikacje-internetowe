package mytld.mycompany.myapp.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mytld.mycompany.myapp.service.DateService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home page.
 */
@Controller("homeController")
//@RequestMapping("/alamakota")
//@Scope("prototype")
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
//	@Inject
	private Map<String, DateService> dateServiceMap; 

//	@Autowired
//	@Qualifier("dateService1")
	@Resource(name="dateService1")
	private DateService dateService1;
	
//	@Autowired(required=false)
////	@Required
//	public void setDateService(DateService dateService) {
//		this.dateService = dateService;
//	}
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/{imie},{nazwisko}"/*, method = RequestMethod.GET*//*, params={"imie", "nazwisko"}, headers={}, produces={}*/)
	public String home(Locale locale, Model model, @PathVariable("imie") String imieP, @PathVariable("nazwisko") String nazwiskoP,
			@RequestParam(defaultValue="") String imie, @RequestParam(defaultValue="") String nazwisko) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		logger.info("imieP {}, naziwskoP {}", imieP, nazwiskoP);
		logger.info("imie {}, naziwsko {}", imie, nazwisko);
		
		logger.info(dateServiceMap.toString());
		
		String formattedDate = dateServiceMap.get("dateService1").getCurrentDate(locale);
		String formattedDate1 = dateService1.getCurrentDate(locale);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	@RequestMapping("/")
	public /*ModelAndView*/ String home2(HttpServletResponse httpServletResponse) {
		
//		return new ModelAndView("home", new HashMap<String, Object>());
		return "home";
	}
	
	@PostConstruct
	public void init() {
		logger.debug("initializing");
	}
	
	@PreDestroy
	public void destroy() {
		logger.debug("destroying");		
	}
	
}
