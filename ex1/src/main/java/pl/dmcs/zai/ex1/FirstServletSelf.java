package pl.dmcs.zai.ex1;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FirstServletSelf extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doGet(req, resp);
		
		List<String> params = (List<String>) req.getParameterNames();
		
		PrintWriter pw = resp.getWriter();
		pw.append("hello world");
		
		pw.flush();
		pw.close();
		
	}
	
}
