package pl.dmcs.zai.home;

import java.security.Principal;
import java.util.Locale;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.dmcs.zai.account.Account;
import pl.dmcs.zai.conversion.Account2;
import pl.dmcs.zai.events.SimpleEvent;
import pl.dmcs.zai.support.web.MessageResolver;
import pl.dmcs.zai.validation.SimpleAccountValidator;

@Controller
public class HomeController implements ApplicationContextAware,
		ApplicationEventPublisherAware {

	private static final Logger log = LoggerFactory
			.getLogger(HomeController.class);

	private ApplicationContext ac;
	private ApplicationEventPublisher applicationEventPublisher;

	@Autowired
	private SimpleAccountValidator validator;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Principal principal,
			@RequestParam(required = false) @Valid Account2 model,
			/*Errors errors,*/ Locale locale) {
		// String msg = ac.getMessage("view.index.title", new Object[] {}/*,
		// "domyslna"*/,
		// new Locale("de")/*locale*/);

		String msg = MessageResolver.getMessage("view.index.title");

		log.info(msg);

		SimpleEvent se = new SimpleEvent(this, msg);
		applicationEventPublisher.publishEvent(se);

		// Account a = new Account("", "", "");
		// ValidationUtils.invokeValidator(validator, a, errors);
		// errors.getErrorCount()
		// log.info("znaleziono błędy: {}", errors.getFieldErrors());

		return principal != null ? "homeSignedIn" : "homeNotSignedIn";
	}

	@RequestMapping(value = "/acc2", method = RequestMethod.GET)
	public String acc2(@RequestParam Account2 acc2) {
		log.info(acc2.getImie() + " - " + acc2.getNazwisko());

		return "homeNotSignedIn";
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.ac = applicationContext;
	}

	@Override
	public void setApplicationEventPublisher(
			ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}
}
