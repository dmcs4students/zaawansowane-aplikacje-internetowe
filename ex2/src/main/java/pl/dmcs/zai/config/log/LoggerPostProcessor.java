package pl.dmcs.zai.config.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

/**
 * Custom BeanPostProcessor which adds a Logger to the class with @Log annotated field
 *
 * @author Robert Ritter
 */
public class LoggerPostProcessor implements BeanPostProcessor {

	private static final Logger log = LoggerFactory.getLogger(LoggerPostProcessor.class);

    public Object postProcessAfterInitialization(Object bean, String beanName) throws
            BeansException {
        return bean;
    }

    public Object postProcessBeforeInitialization(final Object bean, String beanName)
            throws BeansException {
        ReflectionUtils.doWithFields(bean.getClass(), new ReflectionUtils.FieldCallback() {
            public void doWith(Field field) throws IllegalArgumentException,
                    IllegalAccessException {
                ReflectionUtils.makeAccessible(field);

                log.trace("processing bean " + bean.getClass().getSimpleName());

                //Check if the field is annoted with @Log
                if (field.getAnnotation(Log.class) != null) {
                    Logger logger = LoggerFactory.getLogger(bean.getClass());
                    field.set(bean, logger);
                }
            }
        });

        return bean;
    }
}
