package pl.dmcs.zai.config;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import pl.dmcs.zai.config.log.LoggerPostProcessor;

@Configuration
@ComponentScan(basePackages = { "pl.dmcs.zai" })
public class RootConfig {

//	@Autowired
//	private StringAccount2Converter stringAccount2Converter;

	@Bean
	public static PropertyPlaceholderConfigurer propertyPlaceholderConfigurer() {
		PropertyPlaceholderConfigurer ppc = new PropertyPlaceholderConfigurer();
		ppc.setLocation(new ClassPathResource("/persistence.properties"));
		return ppc;
	}
	
	// dodaj logowanie do klas zarządzanych przez Spring
	@Bean
	public LoggerPostProcessor loggerPostProcessor() {
		return new LoggerPostProcessor();
	}

//	@Bean(name = "conversionService")
//	public ConversionServiceFactoryBean configureConversionService() {
//		ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
//
//		Set<Converter> converters = new HashSet<>();
//		converters.add(stringAccount2Converter);
//		bean.setConverters(converters);
//		
//		return bean;
//	}

}