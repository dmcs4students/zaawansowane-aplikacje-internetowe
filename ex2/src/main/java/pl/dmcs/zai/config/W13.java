package pl.dmcs.zai.config;

import java.util.HashMap;
import java.util.Map;

import javax.mail.Session;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import pl.dmcs.zai.controller.w13.ExampleJob;
import pl.dmcs.zai.controller.w13.jms.Receiver;

/**
 * konfiguracja do wykładu 13
 * 
 * @author Robert Ritter
 * 
 */
@Configuration
@EnableAsync
@EnableScheduling
public class W13 {

	private static final Logger log = LoggerFactory.getLogger(W13.class);

	// region Scheduling & Execution
	@Bean
	public ThreadPoolTaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(5);
		executor.setMaxPoolSize(10);
		executor.setQueueCapacity(25);

		return executor;
	}

	@Bean
	public ThreadPoolTaskScheduler taskScheduler() {
		ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
		scheduler.setPoolSize(5);

		return scheduler;
	}

	@Bean
	// @DependsOn
	public JobDetailFactoryBean exampleJob() {
		Map<String, String> jobData = new HashMap<>();
		jobData.put("timeout", "5");

		JobDetailFactoryBean job = new JobDetailFactoryBean();
		job.setJobClass(ExampleJob.class);
		job.setJobDataAsMap(jobData);
		job.setDurability(true);

		return job;
	}

	@Bean
	public SimpleTriggerFactoryBean simpleTrigger() {
		SimpleTriggerFactoryBean trigger = new SimpleTriggerFactoryBean();
		trigger.setJobDetail(exampleJob().getObject());
		trigger.setStartDelay(10000);
		trigger.setRepeatInterval(50000);

		return trigger;
	}

	@Bean
	public CronTriggerFactoryBean cronTrigger() {
		CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
		trigger.setJobDetail(exampleJob().getObject());
		trigger.setCronExpression("0 0 6 * * ?");

		return trigger;
	}

	@Bean
	public SchedulerFactoryBean schedulerFactory() {
		SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
		scheduler.setTriggers(cronTrigger().getObject(), simpleTrigger()
				.getObject());

		return scheduler;

	}

	// endregion

	// region Mail

	@Bean
	public JavaMailSenderImpl mailSender() {
		// Properties javaMailProperties = new Properties();
		// javaMailProperties.setProperty("mail.smtps.auth", "true");
		// javaMailProperties.setProperty("mail.smtp.ssl.enable", "true");
		// javaMailProperties.setProperty("mail.transport.protocol", "smtps");
		// javaMailProperties.setProperty("mail.debug", "true");

		// JavaMailSenderImpl sender = new JavaMailSenderImpl();
		// sender.setDefaultEncoding("UTF-8");
		// sender.setHost("smtp.wp.pl");
		// sender.setPort(465);
		// sender.setUsername("zai-2014");
		// sender.setPassword("AlaMaKota");
		// sender.setProtocol("smtps");
		// sender.setJavaMailProperties(javaMailProperties);
		
/*
 * context.xml
 * 
 * <Resource name="mail/Session" auth="Container" type="javax.mail.Session"
		username="zai-2014" password="AlaMaKota" mail.user="zai-2014"
		mail.password="AlaMaKota" mail.smtps.host="smtp.wp.pl" mail.smtp.port="465"
		mail.debug="true" mail.smtps.auth="true" mail.transport.protocol="smtps"
		mail.smtp.ssl.enable="true" />
 * */		

		log.info("mailSender()");
		Session session = null;
		log.info("heartbeat");
		try {
			Context ctx = new InitialContext();
			log.info("heartbeat");
			session = (Session) ctx.lookup("java:comp/env/mail/Session");
			log.info("heartbeat");
		} catch (Throwable e) {
			log.error(e.getMessage(), e);
		}
		log.info("mailSession: {}", session);

		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setSession(session);

		return sender;
	}

	@Bean
	public SimpleMailMessage templateMessage() {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("zai-2014@wp.pl");
		message.setSubject("Wiadomość z serwisu");

		return message;
	}

	@Bean
	public VelocityEngine velocityEngine() {
		VelocityEngine engine = new VelocityEngine();
		engine.setProperty(VelocityEngine.RESOURCE_LOADER, "class");
		engine.setProperty("class.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");

		return engine;
	}

	// endregion

	// region JMS

	private static final String QUEUE_NAME = "zai-2014";

	@Bean
	public Queue queue() {
		return new Queue(QUEUE_NAME, false);
	}

	@Bean
	public TopicExchange exchange() {
		return new TopicExchange("zai-2014-exchange");
	}

	@Bean
	public Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(QUEUE_NAME);
	}

	@Bean
	public SimpleMessageListenerContainer container(
			ConnectionFactory connectionFactory,
			MessageListenerAdapter listenerAdapter) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames(QUEUE_NAME);
		container.setMessageListener(listenerAdapter);
		return container;
	}

	@Bean
	public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
		RabbitTemplate template = new RabbitTemplate(connectionFactory);
		template.setRoutingKey(QUEUE_NAME);
		return template;
	}

	@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory connectionFactory = new CachingConnectionFactory(
				"localhost");
		connectionFactory.setUsername("guest");
		connectionFactory.setPassword("guest");
		return connectionFactory;
	}

	@Bean
	public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
		RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory);

		return rabbitAdmin;
	}

	@Bean
	public Receiver receiver() {
		return new Receiver();
	}

	@Bean
	public MessageListenerAdapter listenerAdapter(Receiver receiver) {
		return new MessageListenerAdapter(receiver, "receiveMessage");
	}

	// endregion
}
