package pl.dmcs.zai.config.log;

import java.lang.annotation.*;

/**
 * inject LogBack logger for field
 *
 * @author Robert Ritter
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface Log {
}
