package pl.dmcs.zai.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

/**
 * 
 * Klasa konfiguruje uzycie bibliotek szablonujących dla widoku
 * 
 * @author Robert Ritter
 * 
 */
@Configuration
public class TemplatingConfig {

	private static final String TILES = "/WEB-INF/tiles/tiles.xml";
	private static final String VIEWS = "/WEB-INF/views/**/views.xml";

	// @Bean
	// public InternalResourceViewResolver internalResourceViewResolver() {
	// InternalResourceViewResolver vr = new InternalResourceViewResolver();
	// vr.setPrefix("/WEB-INF/views/");
	// vr.setSuffix(".jsp");
	// vr.setViewClass(JstlView.class);
	//
	// return vr;
	// }

	// region Tiles configuration
	@Bean
	public TilesViewResolver configureTilesViewResolver() {
		return new TilesViewResolver();
	}

	@Bean
	public TilesConfigurer configureTilesConfigurer() {
		TilesConfigurer configurer = new TilesConfigurer();
		configurer.setDefinitions(new String[] { TILES, VIEWS });
		return configurer;
	}

	// endregion

	// region Velocity configuration
	// @Bean
	// public VelocityViewResolver configureVelocityViewResolver() {
	// VelocityViewResolver viewResolver = new VelocityViewResolver();
	// viewResolver.setCache(true);
	// viewResolver.setPrefix("");
	// viewResolver.setSuffix(".vm");
	//
	// return viewResolver;
	// }
	//
	// @Bean
	// public VelocityConfigurer configureVelocityConfigurer() {
	// VelocityConfigurer configurer = new VelocityConfigurer();
	// configurer.setResourceLoaderPath("/WEB-INF/velocity/");
	//
	// return configurer;
	// }
	// endregion

	// region Freemarker configuration
	// @Bean
	// public FreeMarkerViewResolver configureFreeMarkerViewResolver() {
	// FreeMarkerViewResolver viewResolver = new FreeMarkerViewResolver();
	// viewResolver.setCache(true);
	// viewResolver.setPrefix("");
	// viewResolver.setSuffix(".ftl");
	//
	// return viewResolver;
	// }
	//
	// @Bean
	// public FreeMarkerConfigurer configureFreeMarkerConfigurer() {
	// FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
	// configurer.setTemplateLoaderPath("/WEB-INF/freemarker/");
	//
	// return configurer;
	// }
	// endregion

	// region Thymeleaf configuration
	// @Bean
	// public ServletContextTemplateResolver configureThymeleafResolver() {
	// ServletContextTemplateResolver resolver = new
	// ServletContextTemplateResolver();
	// resolver.setPrefix("/WEB-INF/templates/");
	// resolver.setSuffix(".html");
	// resolver.setTemplateMode("HTML5");
	// resolver.setCacheable(false);
	//
	// return resolver;
	// }
	//
	// @Bean
	// public SpringTemplateEngine configureThymeleafTemplateEngine() {
	// SpringTemplateEngine engine = new SpringTemplateEngine();
	// engine.setTemplateResolver(configureThymeleafResolver());
	//
	// return engine;
	// }
	//
	// @Bean
	// public ThymeleafViewResolver configureThymeleafViewResolver() {
	// ThymeleafViewResolver resolver = new ThymeleafViewResolver();
	// resolver.setTemplateEngine(configureThymeleafTemplateEngine());
	// // resolver.setOrder(1);
	// // resolver.setViewNames(new String[] { "*.tm" });
	//
	// return resolver;
	// }
	// endregion
}
