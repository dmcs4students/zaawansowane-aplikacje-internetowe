package pl.dmcs.zai.config;

import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.MethodParameter;
import org.springframework.format.FormatterRegistry;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.context.support.ResourceBundleThemeSource;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.theme.CookieThemeResolver;
import org.springframework.web.servlet.theme.ThemeChangeInterceptor;

import pl.dmcs.zai.controller.ExampleInterceptor;
import pl.dmcs.zai.conversion.Account2StringFormatter;
import pl.dmcs.zai.conversion.StringAccount2Converter;

@Configuration
@ComponentScan(basePackages = "pl.dmcs.zai", includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Controller.class))
@Import(TemplatingConfig.class)
public class WebMvcConfig extends WebMvcConfigurationSupport {

	private static final String MESSAGE_SOURCE = "/WEB-INF/i18n/messages";
	private static final String VALIDATION_SOURCE = "/WEB-INF/i18n/ValidationMessages";

	private static final String RESOURCES_HANDLER = "/resources/";
	private static final String RESOURCES_LOCATION = RESOURCES_HANDLER + "**";

	@Override
	public RequestMappingHandlerMapping requestMappingHandlerMapping() {
		RequestMappingHandlerMapping requestMappingHandlerMapping = super
				.requestMappingHandlerMapping();
		requestMappingHandlerMapping.setUseSuffixPatternMatch(false); // /user :
																		// /user.*
		requestMappingHandlerMapping.setUseTrailingSlashMatch(false); // /user :
																		// /user/
		ExampleInterceptor interceptor = new ExampleInterceptor();
		interceptor.setOpeningTime(8);
		interceptor.setClosingTime(20);
		requestMappingHandlerMapping
		// important for internationalization and themes!!!
				.setInterceptors(new Object[] { /*interceptor, */localeChangeInterceptor(), themeChangeInterceptor() });

		return requestMappingHandlerMapping;
	}

	@Bean(name = "messageSource")
	public MessageSource configureMessageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasenames(MESSAGE_SOURCE, VALIDATION_SOURCE);
		messageSource.setCacheSeconds(5);
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	//region internationalization
	@Bean(name = "localeResolver")
	public CookieLocaleResolver localeResolver() {
		CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
		cookieLocaleResolver.setCookieName("clientLanguage");
		cookieLocaleResolver.setCookieMaxAge(100000);
		
		return cookieLocaleResolver;
	}
	
	@Bean(name = "localeChangeInterceptor")
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
		localeChangeInterceptor.setParamName("lang");
		
		return localeChangeInterceptor;
	}
	//endregion
	
	//region theme
	@Bean(name = "themeSource") 
	public ResourceBundleThemeSource themeSource() {
		ResourceBundleThemeSource themeSource = new ResourceBundleThemeSource();
		themeSource.setBasenamePrefix("/themes/");
		
		return themeSource;
	}
	
	@Bean(name = "themeResolver")
	public CookieThemeResolver themeResolver() {
		CookieThemeResolver cookieThemeResolver = new CookieThemeResolver();
		cookieThemeResolver.setCookieName("clientTheme");
		cookieThemeResolver.setDefaultThemeName("medium");
		
		return cookieThemeResolver;
	}
	
	@Bean(name = "themeChangeInterceptor")
	public ThemeChangeInterceptor themeChangeInterceptor() {
		ThemeChangeInterceptor themeChangeInterceptor = new ThemeChangeInterceptor();
		themeChangeInterceptor.setParamName("motyw");
		
		return themeChangeInterceptor;
	}
	//endregion
	
	//region multipart
//	@Bean(name = "multipartResolver")
//	public CommonsMultipartResolver multipartResolver(){
//		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
//		// 10 MB
//		multipartResolver.setMaxUploadSize(10 * 1024 *1024);
//		
//		return multipartResolver;
//	}
	
	@Bean(name = "multipartResolver")
	public StandardServletMultipartResolver multipartResolver(){
		StandardServletMultipartResolver multipartResolver = new StandardServletMultipartResolver();
		
		return multipartResolver;
	}
	//endregion
	
	@Bean(name = "stringAccount2Converter")
	public StringAccount2Converter stringAccount2Converter() {
		StringAccount2Converter converter = new StringAccount2Converter();
		return converter;
	}

	@Bean(name = "account2StringFormatter")
	public Account2StringFormatter account2StringFormatter() {
		Account2StringFormatter formatter = new Account2StringFormatter();
		return formatter;
	}

	@Override
	public Validator getValidator() {
		LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
		validator.setValidationMessageSource(configureMessageSource());
		return validator;
	}

	@Override
	protected void addFormatters(FormatterRegistry registry) {
		registry.addConverter(stringAccount2Converter());
		registry.addFormatter(account2StringFormatter());
		super.addFormatters(registry);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler(RESOURCES_HANDLER).addResourceLocations(
				RESOURCES_LOCATION);
		registry.addResourceHandler("/JspServlet").addResourceLocations(
				"/JspServlet");
	}

	@Override
	public void configureDefaultServletHandling(
			DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Override
	protected void addArgumentResolvers(
			List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(new UserDetailsHandlerMethodArgumentResolver());
	}

	// custom argument resolver inner classes

	private static class UserDetailsHandlerMethodArgumentResolver implements
			HandlerMethodArgumentResolver {

		public boolean supportsParameter(MethodParameter parameter) {
			return UserDetails.class.isAssignableFrom(parameter
					.getParameterType());
		}

		public Object resolveArgument(MethodParameter parameter,
				ModelAndViewContainer modelAndViewContainer,
				NativeWebRequest webRequest, WebDataBinderFactory binderFactory)
				throws Exception {
			Authentication auth = (Authentication) webRequest
					.getUserPrincipal();
			return auth != null && auth.getPrincipal() instanceof UserDetails ? auth
					.getPrincipal() : null;
		}
	}
}
