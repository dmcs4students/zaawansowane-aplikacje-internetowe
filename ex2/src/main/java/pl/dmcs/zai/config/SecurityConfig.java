package pl.dmcs.zai.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;

import pl.dmcs.zai.account.UserService;

@Configuration
@ImportResource(value = "classpath:spring-security-context.xml")
@EnableWebMvcSecurity
public class SecurityConfig {
	
	@Bean
	public UserService userService() {
		return new UserService();
	}

//	@Bean
//	public TokenBasedRememberMeServices rememberMeServices() {
//		return new TokenBasedRememberMeServices("remember-me-key", userService());
//	}
	
	/**
	 * remember me na podstawie danych z bazy
	 * @return usługa odpowiedzialna za zapis danych o uzytkowniku
	 */
	@Bean
	@Autowired
	public PersistentTokenBasedRememberMeServices rememberMeServices(DataSource dataSource) {
		JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
		tokenRepository.setDataSource(dataSource);
		tokenRepository.setCreateTableOnStartup(true);
		
		PersistentTokenBasedRememberMeServices rememberMeServices = 
				new PersistentTokenBasedRememberMeServices("remember-me-key", userService(), tokenRepository);
		
		return rememberMeServices;
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new StandardPasswordEncoder();
	}
}