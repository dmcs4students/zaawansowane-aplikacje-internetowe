package pl.dmcs.zai.w14.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.dmcs.zai.config.log.Log;
import pl.dmcs.zai.w14.domain.Book;
import pl.dmcs.zai.w14.repository.BookRepository;

@Service
@Transactional(readOnly = true)
public class BookServiceImpl implements BookService {
	@Log
	private Logger log;
	private static final int PAGE_SIZE = 50;

	@Autowired
	private BookRepository bookRepository;

	@PostConstruct
	@Transactional
	protected void initialize() {
		List<Book> books = new ArrayList<>();
		for (int i = 0; i < 150; i++) {
			Book book = new Book();
			book.setAuthors(RandomStringUtils.randomAlphabetic(5) + " "
					+ RandomStringUtils.randomAlphabetic(5));
			book.setISBN(RandomStringUtils.randomNumeric(13));
			book.setTitle(RandomStringUtils.randomAlphabetic(10));
			books.add(book);
		}

		bookRepository.save(books);
	}

	public Page<Book> getBooks(Integer pageNumber) {
		PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE,
				Sort.Direction.ASC, "id");
		return bookRepository.findAll(request);
	}

	@Override
	public List<Book> findByTitle(String title) {
		List<Book> books = null;
		try {
			PageRequest request = new PageRequest(1, PAGE_SIZE,
					Sort.Direction.ASC, "id");
//			books = bookRepository.findByTitle(title, request);
//			books = bookRepository.findByTitle(title);
//			books = bookRepository.findByTitleLike(title);
			books = bookRepository.findByTitleLike(title, request).getContent();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return books;
	}

}
