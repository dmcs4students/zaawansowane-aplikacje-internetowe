package pl.dmcs.zai.w14.service;

import java.util.List;

import org.springframework.data.domain.Page;

import pl.dmcs.zai.w14.domain.Book;

public interface BookService {
	Page<Book> getBooks(Integer pageNumber);

	List<Book> findByTitle(String title);
}
