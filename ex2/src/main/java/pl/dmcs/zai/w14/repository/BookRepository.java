package pl.dmcs.zai.w14.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import pl.dmcs.zai.w14.domain.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
	List<Book> findByTitle(String title);
	List<Book> findByTitleLike(String title);
	List<Book> findByTitle(String title, Pageable pageable);
	Page<Book> findByTitleLike(String title, Pageable pageable);
}
