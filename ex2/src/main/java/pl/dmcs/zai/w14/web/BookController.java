package pl.dmcs.zai.w14.web;

import java.util.List;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.dmcs.zai.config.log.Log;
import pl.dmcs.zai.w14.domain.Book;
import pl.dmcs.zai.w14.service.BookService;

@Controller
@RequestMapping("/example/page/")
public class BookController {

	@Log 
	private Logger log;
	@Autowired
	private BookService bookService;
	
	@RequestMapping(value = "/books/{pageNumber}", method = RequestMethod.GET)
	public String getRunbookPage(@PathVariable Integer pageNumber, Model model) {
	    Page<Book> page = bookService.getBooks(pageNumber);

	    int current = page.getNumber() + 1;
	    int begin = Math.max(1, current - 5);
	    int end = Math.min(begin + 10, page.getTotalPages());

	    model.addAttribute("books", page);
	    model.addAttribute("beginIndex", begin);
	    model.addAttribute("endIndex", end);
	    model.addAttribute("currentIndex", current);

	    return "books";
	}
	
	@RequestMapping(value="/books/title/{title}", method = RequestMethod.GET)
	public String getBookByTitle(@PathVariable String title, Model model) {
		log.info("getBookByTitle(\"{}\")", title);
		
		List<Book> books = bookService.findByTitle(title);
		
		log.info("found books: {}", books);
		
		model.addAttribute("books", books);
		
		return "booksByTitle";
	}
}
