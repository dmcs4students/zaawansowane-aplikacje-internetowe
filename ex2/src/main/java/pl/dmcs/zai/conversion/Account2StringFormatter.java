package pl.dmcs.zai.conversion;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;

public class Account2StringFormatter implements Formatter<Account2> {

	@Override
	public String print(Account2 object, Locale locale) {
		return object.getImie() + " " + object.getNazwisko();
	}

	@Override
	public Account2 parse(String text, Locale locale) throws ParseException {
		System.out.println("inside formatter");
		Account2 a = new Account2();
		a.setImie(text.split(" ")[0]);
		a.setNazwisko(text.split(" ")[1]);
		return a;
	}

}
