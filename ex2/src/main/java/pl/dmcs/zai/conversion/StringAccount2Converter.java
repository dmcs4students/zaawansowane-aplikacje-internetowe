package pl.dmcs.zai.conversion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

//@Component
public class StringAccount2Converter implements Converter<String, Account2> {

	private static final Logger log = LoggerFactory
			.getLogger(StringAccount2Converter.class);

	@Override
	public Account2 convert(String source) {
		log.info("inside converter");
		Account2 a = new Account2();
		a.setImie(source.split(" ")[0]);
		a.setNazwisko(source.split(" ")[1]);
		return a;
	}

}
