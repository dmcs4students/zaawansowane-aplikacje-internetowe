package pl.dmcs.zai.conversion;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.NotEmpty;

import pl.dmcs.zai.validation.PodzielnePrzez;

public class Account2 {
	@NotEmpty
	private String imie;
	private String nazwisko;
	@PodzielnePrzez(value = 2)
	private int liczba;

	public Account2() {
	}

	public Account2(String imie, String nazwisko, int liczba) {
		super();
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.liczba = liczba;
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	
	public int getLiczba() {
		return liczba;
	}

	public void setLiczba(int liczba) {
		this.liczba = liczba;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
