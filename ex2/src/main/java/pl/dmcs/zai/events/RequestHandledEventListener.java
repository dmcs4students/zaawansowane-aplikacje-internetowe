package pl.dmcs.zai.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.RequestHandledEvent;

//@Service
public class RequestHandledEventListener implements ApplicationListener<RequestHandledEvent> {

	private static final Logger log = LoggerFactory.getLogger(RequestHandledEventListener.class);
	
	@Override
	public void onApplicationEvent(RequestHandledEvent event) {
		log.info("Request with session id {} was handled", event.getSessionId());
	}

}
