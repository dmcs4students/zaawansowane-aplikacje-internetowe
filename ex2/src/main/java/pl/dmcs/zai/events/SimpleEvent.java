package pl.dmcs.zai.events;

import org.springframework.context.ApplicationEvent;

public class SimpleEvent extends ApplicationEvent {

	private String info;
	
	public SimpleEvent(Object source, String info) {
		super(source);
		this.info = info;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

}
