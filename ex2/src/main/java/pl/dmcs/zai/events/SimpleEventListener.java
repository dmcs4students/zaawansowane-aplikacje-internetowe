package pl.dmcs.zai.events;

import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public class SimpleEventListener implements ApplicationListener<SimpleEvent>,
		ApplicationContextAware {

	private static final Logger log = LoggerFactory
			.getLogger(SimpleEventListener.class);

	private ApplicationContext applicationContext;

	@Override
	public void onApplicationEvent(SimpleEvent event) {
		log.info("Custom event handled - {}", event.getInfo());

		Resource resource = applicationContext
				.getResource("classpath:/persistence.properties");
		try {
			String file = FileUtils.readFileToString(resource.getFile());
			log.info(file);
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}

	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;

	}

}