package pl.dmcs.zai.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import pl.dmcs.zai.account.Account;
import pl.dmcs.zai.conversion.Account2;
import pl.dmcs.zai.validation.SimpleAccountValidator;

@Controller
@RequestMapping("/example/modelAttribute/")
@SessionAttributes("osoba")
public class ModelAttributeExampleController {

	private static final Logger log = LoggerFactory
			.getLogger(ModelAttributeExampleController.class);

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setDisallowedFields("imie");
		binder.setValidator(new SimpleAccountValidator());
	}
	
	@ModelAttribute("accountDefault")
	public Account addAccount() {
		return new Account("ala@niepodam.pl", "haslo", "USER");
	}

	@RequestMapping("view1")
	public String view1(@RequestBody String body,
			@RequestHeader("Accept-Encoding") String encoding,
			HttpServletResponse response, @CookieValue(required=false, defaultValue="", value="ex2") String ex2) {
		log.debug("body {}", body);
		log.debug("encoding {}", encoding);
		log.debug("cookie {}", ex2);
		response.addCookie(new Cookie("ex2", "alamakota")/* .setMaxAge(-1); *//*
																			 * .
																			 * setDomain
																			 * (
																			 * "/"
																			 * )
																			 * ;
																			 */);
		return "example/view1";
	}

	@RequestMapping(value = "view3", method = { RequestMethod.GET })
	public String view3Get(Model model, @RequestParam String param) {
		model.addAttribute(new Account2("Bob", "Budowniczy", 1));
		return "example/view3";
	}

	@RequestMapping(value = "view3", method = { RequestMethod.POST })
	public String view3Post(@ModelAttribute Account2 account2,
			@RequestBody String body) {

		log.debug("body {}", body);
		log.debug(account2.toString());

		return "example/view3";
	}

	@RequestMapping(value = "view2", method = { RequestMethod.GET })
	public String view2Get(Model model) {
		model.addAttribute("account2", new Account2());
		return "example/view2";
	}

	@RequestMapping(value = "view2", method = { RequestMethod.POST })
	public String view2Post(@Valid Account2 account2,
			BindingResult bindingResult,
			SessionStatus sessionStatus) {
		log.debug("errors? {}", bindingResult.hasErrors());
		log.debug(account2.toString());
		return "example/view2";
	}

	@RequestMapping(value = "view4"/* produces={""} */)
	// @ResponseBody
	public ResponseEntity<String> view4() {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.APPLICATION_JSON);
		return new ResponseEntity<String>("{\"login\":\"ok\"}",
				responseHeaders, HttpStatus.OK);
	}
}
