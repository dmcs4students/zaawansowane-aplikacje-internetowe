package pl.dmcs.zai.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;

import pl.dmcs.zai.config.log.Log;
import pl.dmcs.zai.controller.dto.Customer;
import pl.dmcs.zai.controller.dto.Mud;

@Controller
@RequestMapping("/example/views/")
public class VelocityViewController {
	@Log
	private static Logger log;
//	@Autowired
	private VelocityConfigurer velocityConfigurer;

	@RequestMapping("/welcome1.vm")
	public void velocity1(Model model, HttpServletResponse httpServletResponse)
			throws IOException {
		log.info("velocity template over stream");

		createVelocityModel(model);

		VelocityEngine velocityEngine = velocityConfigurer.getVelocityEngine();
		String view = VelocityEngineUtils.mergeTemplateIntoString(
				velocityEngine, "velocity/welcome-code.vm", "UTF-8", model.asMap());

		PrintWriter writer = httpServletResponse.getWriter();
		writer.write(view);
		writer.close();
	}

	@RequestMapping("/welcome2.vm")
	public String velocity2(Model model) {
		log.info("velocity template from view");

		createVelocityModel(model);

		return "welcome";
	}

	private void createVelocityModel(Model model) {
		Customer customer = new Customer();
		customer.setName("Bob");
		List<Mud> muds = new ArrayList<>();
		for (int i = 0; i < 10; i++)
			muds.add(new Mud(RandomStringUtils.randomAlphanumeric(3)));

		model.addAttribute("customer", customer);
		model.addAttribute("mudsOnSpecial", muds);
	}

}
