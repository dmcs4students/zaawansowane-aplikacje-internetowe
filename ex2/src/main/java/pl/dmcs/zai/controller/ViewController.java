package pl.dmcs.zai.controller;

import java.util.Date;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.dmcs.zai.config.log.Log;
import pl.dmcs.zai.controller.dto.Product;

@Controller
@RequestMapping("/example/views/")
public class ViewController {
	@Log
	private static Logger log;

	@RequestMapping("/welcome")
	public String tiles(Model model) {
		log.info("tiles view");

		return "default1";
	}

	@RequestMapping("/welcome.tm")
	public String thymeleaf(Model model) {
		log.info("thymeleaf template");

		model.addAttribute(
				"product",
				new Product(RandomStringUtils.randomAlphanumeric(10),
						RandomUtils.nextInt(1, 10), new Date(RandomUtils
								.nextLong((long) (new Date().getTime() - 1e5),
										new Date().getTime()))));

		return "welcome";
	}
}
