package pl.dmcs.zai.controller.dto;

import javax.servlet.http.Part;

import org.springframework.web.multipart.MultipartFile;

public class UploadedFile {
	private String customFileName;
	private MultipartFile file;
	private Part filePart;

	public String getCustomFileName() {
		return customFileName;
	}

	public void setCustomFileName(String customFileName) {
		this.customFileName = customFileName;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
}
