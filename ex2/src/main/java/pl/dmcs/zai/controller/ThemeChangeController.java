package pl.dmcs.zai.controller;

import java.util.Locale;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.dmcs.zai.config.log.Log;

@Controller
@RequestMapping("/example/theme/")
public class ThemeChangeController {

	@Log
	public static Logger log;
	
	@RequestMapping("/")
	public String main(Model model, Locale locale) {
		log.debug("locale set to {}", locale);
		model.addAttribute("selectedLocale", locale);
		
		return "themeHome";
	}
}
