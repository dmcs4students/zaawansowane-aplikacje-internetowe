package pl.dmcs.zai.controller.dto;

public class Mud {
	private String name;

	public Mud(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
