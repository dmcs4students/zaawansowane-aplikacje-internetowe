package pl.dmcs.zai.controller.javascript;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.dmcs.zai.config.log.Log;

@Controller
@RequestMapping("/example/js/")
public class KnockoutController {

	@Log
	private static Logger log;

	@RequestMapping("/knockout")
	public String knockout(Model model) {
		log.info("knockout view");

		return "home/knockout2";
	}

	@RequestMapping("/knockout-tasks")
	public String tasksView(Model model) {
		log.info("knockout tasks view");

		return "home/knockout-tasks";
	}
	
	@RequestMapping(value = "/tasks", produces = "application/json", method=RequestMethod.GET)
	public @ResponseBody String tasksGet() {
		log.info("sending tasks as JSON");
		
		String tasks = "[{\"title\":\"Wire the money to Panama\",\"isDone\":true},{\"title\":\"Get hair dye, beard trimmer, dark glasses and 'passport'\",\"isDone\":false},{\"title\":\"Book taxi to airport\",\"isDone\":false},{\"title\":\"Arrange for someone to look after the cat\",\"isDone\":false}]";

		return tasks;
	}
	
	@RequestMapping(value = "/tasks", method=RequestMethod.POST)
	public @ResponseBody String tasksPost(@RequestBody String tasks, HttpServletRequest request) {
		log.info("receiving tasks as JSON {}", tasks);
		
		try {
			log.info("body {}", IOUtils.readLines(request.getReader()));
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
		
		return "received";
	}
}
