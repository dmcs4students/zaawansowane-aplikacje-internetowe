package pl.dmcs.zai.controller;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import pl.dmcs.zai.config.log.Log;
import pl.dmcs.zai.controller.dto.UploadedFile;
import pl.dmcs.zai.error.AlaMaKotaException;

@Controller
@RequestMapping("/example/upload/")
public class FileUploadController {

	@Log
	public static Logger log;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String get(Model model) {
		log.debug("showing upload form");
		model.addAttribute("uploadedFile", new UploadedFile());
		
		return "uploadGet";
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String post(Model model, @ModelAttribute UploadedFile uploadedFile) throws AlaMaKotaException {
		log.debug("file {} uploaded", uploadedFile.getCustomFileName());
		log.debug("file original name {}", uploadedFile.getFile().getOriginalFilename());

		if (uploadedFile.getCustomFileName().equals("aaa")) {
			throw new AlaMaKotaException();
		}
		
		return "uploadPost";
	}
	
	/**
	 * 
POST /ex2/example/upload/rest
Content-Type: multipart/mixed

--edt7Tfrdusa7r3lNQc79vXuhIIMlatb7PQg7Vp
Content-Disposition: form-data; name="meta-data"
Content-Type: application/json; charset=UTF-8
Content-Transfer-Encoding: 8bit

{
	"name": "value"
}
--edt7Tfrdusa7r3lNQc79vXuhIIMlatb7PQg7Vp
Content-Disposition: form-data; name="file-data"; filename="file.properties"
Content-Type: text/xml
Content-Transfer-Encoding: 8bit
... File Data ...
	 */
	
	@RequestMapping(value="/rest", method = RequestMethod.POST)
	public String onSubmit(@RequestPart("meta-data") String metadata,
	        @RequestPart("file-data") MultipartFile file) {

	    // ...

		return "accepted";
	}
}
