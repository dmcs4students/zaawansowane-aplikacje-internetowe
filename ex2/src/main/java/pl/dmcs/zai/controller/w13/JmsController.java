package pl.dmcs.zai.controller.w13;

import org.slf4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.dmcs.zai.config.log.Log;

@Controller
@RequestMapping("/example/jms/")
public class JmsController {
	
	@Log
	private Logger log;
	
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
	@RequestMapping("1")
	public @ResponseBody String jms1() {
		
		log.info("sending jms message");
		
		rabbitTemplate.convertAndSend("Hello from RabbitMQ");
		
		return "message sent";
	}

}
