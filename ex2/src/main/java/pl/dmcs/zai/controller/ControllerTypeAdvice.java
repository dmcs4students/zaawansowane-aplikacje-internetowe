package pl.dmcs.zai.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import pl.dmcs.zai.conversion.Account2;

@ControllerAdvice(assignableTypes = {ModelAttributeExampleController.class})
public class ControllerTypeAdvice {

	@ModelAttribute("userAcc")
	public Account2 addUserAcc() {
		return new Account2("Bob", "Budowniczy", 2);
	}
}
