package pl.dmcs.zai.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import pl.dmcs.zai.config.log.Log;
import pl.dmcs.zai.controller.dto.Animal;
import pl.dmcs.zai.controller.dto.LatestProduct;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Controller
@RequestMapping("/example/views/")
public class FreemarkerViewController {
	@Log
	private static Logger log;
	// @Autowired
	private FreeMarkerConfigurer freeMarkerConfigurer;

	@RequestMapping("/welcome1.ftl")
	public void freemarker1(Model model, HttpServletResponse httpServletResponse)
			throws IOException, TemplateException {
		log.info("freemarker template over stream");

		createFreemarkerModel(model);

		Template template = freeMarkerConfigurer.createConfiguration()
				.getTemplate("welcome-code.ftl", Locale.ENGLISH, "UTF-8", true);
		String view = FreeMarkerTemplateUtils.processTemplateIntoString(
				template, model.asMap());

		PrintWriter writer = httpServletResponse.getWriter();
		writer.write(view);
		writer.close();
	}

	@RequestMapping("/welcome2.ftl")
	public String freemarker2(Model model) {
		log.info("freemarker template with view");

		createFreemarkerModel(model);

		return "welcome";

	}

	private void createFreemarkerModel(Model model) {
		LatestProduct latestProduct = new LatestProduct("green mouse",
				"products/greenmouse.html");

		model.addAttribute("user", "Ala");
		model.addAttribute("latestProduct", latestProduct);
		model.addAttribute("animals", generateAnimals(10));
	}

	private List<Animal> generateAnimals(int amount) {
		List<Animal> animals = new ArrayList<Animal>();

		for (int i = 0; i < amount; i++) {
			animals.add(new Animal());
		}

		return animals;
	}

}
