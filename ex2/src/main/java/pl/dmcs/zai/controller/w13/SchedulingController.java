package pl.dmcs.zai.controller.w13;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.PeriodicTrigger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.dmcs.zai.config.log.Log;

@Controller
@RequestMapping("/example/job/")
public class SchedulingController {

	@Log
	private Logger log;

	@Autowired
	private TaskExecutor taskExecutor;
	@Autowired
	private TaskScheduler taskScheduler;

	@RequestMapping("javaEE")
	public @ResponseBody
	String javaEE() throws InterruptedException, ExecutionException {
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		Future<String> result = executorService.submit(new Callable<String>() {

			@Override
			public String call() throws Exception {
				Thread.sleep(1000);
				log.info("ala");
				return "alamakota";
			}

		}
		/*
		 * new Runnable() {
		 * 
		 * @Override public void run() { log.info("javaEE executor");
		 * 
		 * } }
		 */);
		// String r = result.get();
		// log.info(r);
		log.info("po ala");

//		executorService = Executors.newScheduledThreadPool(1);
//		((ScheduledExecutorService) executorService).schedule(runable, 10,
//				TimeUnit.MINUTES);

		executorService.shutdown();
		executorService.awaitTermination(60, TimeUnit.SECONDS);

		return "javaEE executor";
	}

	@RequestMapping("taskExecutorExample")
	public @ResponseBody
	String taskExecutorExample() {
		log.info("taskExecutorExample");

		for (int i = 0; i < 25; i++) {
			taskExecutor.execute(new MessagePrinterTask("Message" + i));
		}

		log.info("tasks executed");

		return "task executed";
	}

	@RequestMapping("periodicExecutorExample")
	public @ResponseBody
	String periodicExecutorExample() {
		log.info("periodicExecutorExample");

		taskScheduler.schedule(new MessagePrinterTask("Periodic message CRON"),
				new CronTrigger("* 15 9-17 * * MON-FRI"));
		taskScheduler.schedule(
				new MessagePrinterTask("Periodic message TIMER"),
				new PeriodicTrigger(2, TimeUnit.SECONDS));

		log.info("periodic tasks executed");

		return "periodic task executed";
	}

	@RequestMapping("asyncExample")
	public @ResponseBody
	String asyncExample() {
		log.info("asyncExample");

		Future<String> future = returnSomething(0);

		log.info("async tasks executed");

		return "async task executed";
	}

//	 @Scheduled(fixedRate = 5000, initialDelay=10)
	public void doSomething() {
		// something that should execute periodically
		log.info("do something every 5s");
	}

//	 @Scheduled(cron = "*/5 * * * * MON-FRI")
	public void doSomethingElse() {
		// something that should execute on weekdays only
		log.info("do something else every 5s");
	}

	@Async("taskExecutor")
	public Future<String> returnSomething(int i) {
		// this will be executed asynchronously
		for (int j = 0; j < Short.MAX_VALUE; i++) {
			i++;
		}

		log.info("return something finished");
		return new AsyncResult<String>(i + "");
	}

	private class MessagePrinterTask implements Runnable {

		private String message;

		public MessagePrinterTask(String message) {
			this.message = message;
		}

		public void run() {
			log.info(message);
		}

	}
}
