package pl.dmcs.zai.controller;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.dmcs.zai.config.log.Log;

@Controller
@RequestMapping("/example/security/")
public class SecurityInformationController {

	@Log
	private static Logger log;

	@RequestMapping("/")
	public String get() {
		SecurityContext sc = SecurityContextHolder.getContext();
		log.info("authority: {}", ToStringBuilder.reflectionToString(
				sc.getAuthentication(), ToStringStyle.MULTI_LINE_STYLE));

		return "homeNotSignedIn";
	}
	
	@RequestMapping("/2")
	public String get2(@AuthenticationPrincipal User currentUser) {
		
		log.info("authority: {}", currentUser);

		return "homeNotSignedIn";
	}
}
