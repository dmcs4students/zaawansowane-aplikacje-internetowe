package pl.dmcs.zai.controller.dto;

import java.util.Random;

public class Animal {

	private String name;
	private int price;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Animal(String name, int price) {
		this.name = name;
		this.price = price;
	}

	public Animal() {
		this.name = "aaa";/*RandomStringUtils.randomAlphabetic(10);*/
		this.price = new Random().nextInt();
	}

}
