package pl.dmcs.zai.controller.dto;

import java.util.Random;

public class Customer {

	private Random random = new Random();
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean hasPurchased(Mud mud) {
		return random.nextBoolean();
	}
	
}
