package pl.dmcs.zai.controller.w13.jms;

import org.slf4j.Logger;

import pl.dmcs.zai.config.log.Log;

public class Receiver {
	@Log
	private Logger log;
	
//	@Autowired
//	private AnnotationConfigApplicationContext context;

	public void receiveMessage(String message) {
        log.info("Received <{}>", message);
//        this.context.close();
    }
}
