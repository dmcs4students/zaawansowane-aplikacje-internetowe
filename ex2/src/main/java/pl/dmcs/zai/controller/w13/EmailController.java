package pl.dmcs.zai.controller.w13;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.dmcs.zai.config.log.Log;

@Controller
@RequestMapping("/example/email/")
public class EmailController {

	@Log
	private Logger log;

	@Autowired
	private SimpleMailMessage templateMessage;
	@Autowired
	private MailSender mailSender;
	@Autowired
	private VelocityEngine velocityEngine;

	@RequestMapping("1")
	public @ResponseBody
	String mail1() {
		log.info("first mail example");

		SimpleMailMessage msg = new SimpleMailMessage(this.templateMessage);
		msg.setTo("zai-2014@wp.pl");
		msg.setText("Wiadomość z ważnymi danymi pobranymi z bazy danych " + msg);
		try {
			this.mailSender.send(msg);
		} catch (MailException ex) {
			// simply log it and go on...
			System.err.println(ex.getMessage());
		}

		return "mail sent";
	}

	@RequestMapping("2")
	public @ResponseBody
	String mail2() {
		log.info("second mail example");

		MimeMessagePreparator preparator = new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {

				mimeMessage.setRecipient(Message.RecipientType.TO,
						new InternetAddress("zai-2014@wp.pl"));
				mimeMessage.setFrom(new InternetAddress("zai-2014@wp.pl"));
				mimeMessage
						.setText("Dear "
								+ "ala"
								+ " "
								+ "makota"
								+ ", thank you for placing order. Your order number is "
								+ 123);
			}
		};

		try {
			((JavaMailSender) this.mailSender).send(preparator);
		} catch (MailException ex) {
			// simply log it and go on...
			System.err.println(ex.getMessage());
		}

		return "mail sent";
	}

	@RequestMapping("3")
	public @ResponseBody
	String mail3() throws MessagingException {
		log.info("second mail example");

		JavaMailSender sender = (JavaMailSender) mailSender;

		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setFrom("zai-2014@wp.pl");
		helper.setTo("zai-2014@wp.pl");
		helper.setText(
				"<html><body><img src=cid:identifier1234></body></html>", true);
		FileSystemResource file = new FileSystemResource(new File(
				"D:\\Pobrane - WWW\\PocztaWP - 1 nowych.png"));

		helper.addAttachment("strona.jpg", file);
		helper.addInline("identifier1234", file);

		sender.send(message);

		return "mail sent";
	}

	@RequestMapping("4")
	public @ResponseBody
	String mail4() throws MessagingException {
		log.info("fourth mail example");

		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
				message.setTo("zai-2014@wp.pl");
				message.setFrom("zai-2014@wp.pl"); // could be parameterized...
				Map<String, Object> model = new HashMap<>();
				model.put("user", "alamakota");
				model.put("mail", "alamakota@wp.pl");
				String text = VelocityEngineUtils.mergeTemplateIntoString(
						velocityEngine,
						"pl/dmcs/zai/emails/registration-confirmation.vm",
						"UTF-8", model);
				message.setText(text, true);
			}
		};
		((JavaMailSender) this.mailSender).send(preparator);

		return "mail sent";
	}
}
