package pl.dmcs.zai.support.web;

import java.util.Locale;

public class MessageResolver {

	private MessageResolver() {}
	
	public static String getMessage(String code) {
		return ApplicationContextProvider.getAC().getMessage(code,  null, Locale.getDefault());
	}
	
}
