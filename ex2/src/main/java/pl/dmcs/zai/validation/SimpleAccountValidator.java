package pl.dmcs.zai.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.dmcs.zai.account.Account;

@Component
public class SimpleAccountValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Account.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
//		errors.pushNestedPath("address");
//		ValidationUtils.invokeValidator(validator, obj, errors);
//		errors.popNestedPath();
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "field.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "field.required");
		
		Account a = (Account) target;
		if (a.getEmail().equals("ala@makota.pl")) {
			errors.reject("email", "field.required");
		}
	}

}
