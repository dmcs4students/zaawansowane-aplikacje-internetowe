package pl.dmcs.zai.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PodzielnePrzezValidator implements ConstraintValidator<PodzielnePrzez, Integer> {

	private int wartosc;
	
	@Override
	public void initialize(PodzielnePrzez constraintAnnotation) {
		this.wartosc = constraintAnnotation.value();
	}

	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext context) {
		if (value == null)
			return false;
		else 
			return (value % wartosc) == 0;
	}

}
