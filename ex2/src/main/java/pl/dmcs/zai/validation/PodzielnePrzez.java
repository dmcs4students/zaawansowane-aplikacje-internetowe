package pl.dmcs.zai.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.METHOD, ElementType.FIELD })
@Constraint(validatedBy=PodzielnePrzezValidator.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface PodzielnePrzez {
String message() default "{validation.niedzielisie}" /*"Nie dzieli się"*/;
Class<?>[] groups() default {};
Class<? extends Payload>[] payload() default {};

int value() default 1;
}
