package pl.dmcs.zai.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class JspServlet
 */
@WebServlet(urlPatterns={ "/JspServlet"/*, "/*", "/ala/**", "*.html"*/})
public class JspServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(JspServlet.class);
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public JspServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		log.debug("JspServlet");
		
		request.setAttribute("a", request);
		
		RequestDispatcher rd =  request.getRequestDispatcher("/WEB-INF/jsp/template.jsp");
		if (rd != null) {
			rd.include(request, response);
		}
		
		log.debug("JspServlet koniec");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
