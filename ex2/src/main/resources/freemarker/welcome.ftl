		
	
<html>
<head>
  <title>Welcome!</title>
</head>
<body>
  <h1>Welcome ${user}!</h1>
  <p>Our latest product:
  <a href="${latestProduct.url}">${latestProduct.name}</a>!
  
  <br/>
  
  <ul>
<#list animals as being>

  <li>${being} ${being.name} for ${being.price} Euros</li>
  <#if user == "Big Joe">
     (except for you)
	</#if>
	</#list> <#-- WRONG! The "if" has to be closed first. -->
</ul>  
</body>
</html>  