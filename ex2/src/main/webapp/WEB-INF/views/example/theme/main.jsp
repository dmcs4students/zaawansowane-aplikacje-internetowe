<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<s:theme code="akapitCode" var="akapit" />
<p style="${akapit}">
	Wybrany język to ${selectedLocale} 	
	<br/>
	<s:message code="locale.test" />
</p>