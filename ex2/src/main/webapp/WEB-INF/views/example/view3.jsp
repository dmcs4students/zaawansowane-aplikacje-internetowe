<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<p>
	${userAcc}
</p>

<f:form method="post" modelAttribute="account2">
	<f:input path="imie" /><f:errors path="imie"/><br/>
	<!-- <input name="imie" id="imie" /> -->
	<f:input path="nazwisko" /><f:errors path="nazwisko"/><br/>
	<f:input path="liczba" /><f:errors path="liczba" /><br/>
	
	<f:button type="submit"><s:message code="button.submit" /></f:button>
	<button type="button">Przycisk</button>
</f:form>