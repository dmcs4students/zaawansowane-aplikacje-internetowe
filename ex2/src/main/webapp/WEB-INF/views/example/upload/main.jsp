<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<h1>Upload file</h1>
<f:form method="POST" enctype="multipart/form-data" modelAttribute="uploadedFile">
	<f:input path="customFileName" />
	<f:input path="file" type="file" />
	<f:button type="submit">Upload</f:button>
</f:form>