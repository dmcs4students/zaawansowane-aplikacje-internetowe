<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="container-fluid">

	<div class="row">
		<div class="col-md-2">
			<p></p>
		</div>
		<div class="books col-md-8">
			<table>
				<thead>
					<tr>
						<th>Authors</th>
						<th>Title</th>
						<th>ISBN</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${books}" var="book">
						<tr>
							<td>${book.authors}</td>
							<td>${book.title}</td>
							<td>${book.ISBN}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="col-md-2">
			<p></p>
		</div>

	</div>

</div>