<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:url var="firstUrl" value="/example/page/books/1" />
<c:url var="lastUrl" value="/example/page/books/${books.totalPages}" />
<c:url var="prevUrl" value="/example/page/books/${currentIndex - 1}" />
<c:url var="nextUrl" value="/example/page/books/${currentIndex + 1}" />

<div class="container-fluid">

	<div class="row">
		<div class="col-md-2">
			<p></p>
		</div>
		<div class="books col-md-8">
			<table>
				<thead>
					<tr>
						<th>Authors</th>
						<th>Title</th>
						<th>ISBN</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${books.content}" var="book">
						<tr>
							<td>${book.authors}</td>
							<td>${book.title}</td>
							<td>${book.ISBN}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="col-md-2">
			<p></p>
		</div>

	</div>

	<div class="row">
		<div class="col-md-2">
			<p></p>
		</div>
		<div class="pagination col-md-8">
			<ul>
				<c:choose>
					<c:when test="${currentIndex == 1}">
						<li class="disabled"><a href="#">&lt;&lt;</a></li>
						<li class="disabled"><a href="#">&lt;</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="${firstUrl}">&lt;&lt;</a></li>
						<li><a href="${prevUrl}">&lt;</a></li>
					</c:otherwise>
				</c:choose>
				<c:forEach var="i" begin="${beginIndex}" end="${endIndex}">
					<c:url var="pageUrl" value="/example/page/books/${i}" />
					<c:choose>
						<c:when test="${i == currentIndex}">
							<li class="active"><a href="${pageUrl}"><c:out
										value="${i}" /></a></li>
						</c:when>
						<c:otherwise>
							<li><a href="${pageUrl}"><c:out value="${i}" /></a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${currentIndex == books.totalPages}">
						<li class="disabled"><a href="#">&gt;</a></li>
						<li class="disabled"><a href="#">&gt;&gt;</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="${nextUrl}">&gt;</a></li>
						<li><a href="${lastUrl}">&gt;&gt;</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
		<div class="col-md-2">
			<p></p>
		</div>

	</div>

</div>