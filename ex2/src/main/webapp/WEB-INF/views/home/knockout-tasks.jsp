<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>



<div class="hero-unit">
	<h1>
		<s:message code="view.index.title" />
	</h1>

	<h3>Tasks</h3>

	<form data-bind="submit: addTask">
		Add task: <input data-bind="value: newTaskText"
			placeholder="What needs to be done?" />
		<button type="submit">Add</button>
	</form>

	<ul data-bind="foreach: tasks, visible: tasks().length > 0">
		<li><input type="checkbox" data-bind="checked: isDone" /> <input
			data-bind="value: title, disable: isDone" /> <a href="#"
			data-bind="click: $parent.removeTask">Delete</a></li>
	</ul>

	You have <b data-bind="text: incompleteTasks().length">&nbsp;</b>
	incomplete task(s) <span
		data-bind="visible: incompleteTasks().length == 0"> - it's beer
		time!</span>

	<button data-bind="click: save">Save</button>


	<c:url value="/example/js/tasks" var="tasksURL" />

	<script type="text/javascript">
		$(function() {

			function Task(data) {
				this.title = ko.observable(data.title);
				this.isDone = ko.observable(data.isDone);
			}

			function TaskListViewModel() {
				// Data
				var self = this;
				self.tasks = ko.observableArray([]);
				self.newTaskText = ko.observable();
				self.incompleteTasks = ko.computed(function() {
					return ko.utils.arrayFilter(self.tasks(), function(task) {
						return !task.isDone() && !task._destroy
					});
				});

				// Operations
				self.addTask = function() {
					self.tasks.push(new Task({
						title : this.newTaskText()
					}));
					self.newTaskText("");
				};
				self.removeTask = function(task) {
					self.tasks.destroy(task);
				};
				self.save = function() {
					$.ajax("${tasksURL}", {
						data : ko.toJSON({
							tasks : self.tasks
						}),
						type : "post",
						contentType : "application/json",
						success : function(result) {
							console.log(result);
						}
					});
				};

				// Load initial state from server, convert it to Task instances, then populate self.tasks
				$.getJSON("${tasksURL}", function(allData) {
					var mappedTasks = $.map(allData, function(item) {
						return new Task(item)
					});
					self.tasks(mappedTasks);
				});
				/* $.ajax({
					dataType : "json",
					url : "${tasksURL}",
					success : function(allData) {
						var mappedTasks = $.map(allData, function(item) {
							return new Task(item)
						});
						self.tasks(mappedTasks);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						console.log("textStatus ", textStatus);
						console.log("errorThrown ", errorThrown);
					}
				}); */
			}

			ko.applyBindings(new TaskListViewModel());
		});
	</script>
</div>