<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<div class="hero-unit">
	<h1>
		<s:message code="view.index.title" />
	</h1>


	<p>
		First name: <strong data-bind="text: firstName"></strong>
	</p>
	<p>
		Last name: <strong data-bind="text: lastName"></strong>
	</p>

	<p>
		First name: <input data-bind="value: firstName" />
	</p>
	<p>
		Last name: <input data-bind="value: lastName" />
	</p>
	
	<p>Full name: <strong data-bind="text: fullName"></strong></p>
	
	<button data-bind="click: capitalizeLastName">Go caps</button>

</div>

<script type="text/javascript">
	$(function() {
		// This is a simple *viewmodel* - JavaScript that defines the data and behavior of your UI
		function AppViewModel() {
			this.firstName = ko.observable("Bert");
		    this.lastName = ko.observable("Bertington");
		    
		    this.fullName = ko.computed(function() {
		        return this.firstName() + " " + this.lastName();    
		    }, this);
		    
		    this.capitalizeLastName = function() {
		        var currentVal = this.lastName();        // Read the current value
		        this.lastName(currentVal.toUpperCase()); // Write back a modified value
		    }; 
		}

		// Activates knockout.js
		ko.applyBindings(new AppViewModel());
	});

	
</script>