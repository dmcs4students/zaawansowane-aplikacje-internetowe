<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<div class="hero-unit">
	<h1>
		<s:message code="view.index.title" />
	</h1>

	<div id="outer" style="width: 300px; height: 300px; background-color: #aaa;">
		<button type="button" id="kliknijMnie">Kliknij mnie</button>
		<button type="button" id="przycisk">Przycisk</button>
		<button type="button" id="przycisk1">Przycisk1</button>
		<br/>
		<button type="button" id="przycisk2">Przycisk2</button>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$("#kliknijMnie").hide();
		$("#przycisk").hide();
		$("#przycisk1").hide();
		$("#przycisk2").hide();
		
		$("#outer").click(function() {
			console.log("div clicked");
		});
		
		$("#outer").hover(function() {
			console.log("div hovered");
		});
		
		$("#outer").hover(function() {
			$("#przycisk1").fadeToggle(500);
			//$("#przycisk2").fadeToggle(500).toggleClass("greenBg");
			$("#przycisk2").fadeToggle(500, function() {
				$(this).toggleClass("greenBg");
			});
		});
		
		$("#outer").mouseleave(function() {
			$("#kliknijMnie").hide("slow");
			$("#przycisk").slideUp(500);
		});
		$("#outer").mouseenter(function() {
			$("#kliknijMnie").show(1500);
			$("#przycisk").slideDown(500);
		});

		$("#kliknijMnie").click(function(evnt) {
			//evnt.stopPropagation();
			evnt.preventDefault();
			console.log("kliknięto ", this);
		});

		$("#kliknijMnie").on('click', function() {
			console.log("kliknięto 2 ", this);
		});
		
		
		
		var response;
		 
		$.get( "foo.php", function( r ) {
		    response = r;
		});
		 
		console.log( response ); // undefined
		
		$.ajax({
		    // the URL for the request
		    url: "post.php",
		 
		    // the data to send (will be converted to a query string)
		    data: {
		        id: 123
		    },
		 
		    // whether this is a POST or GET request
		    type: "GET",
		 
		    // the type of data we expect back
		    dataType : "json",
		 
		    // code to run if the request succeeds;
		    // the response is passed to the function
		    success: function( json ) {
		        $( "<h1/>" ).text( json.title ).appendTo( "body" );
		        $( "<div class=\"content\"/>").html( json.html ).appendTo( "body" );
		    },
		 
		    // code to run if the request fails; the raw request and
		    // status codes are passed to the function
		    error: function( xhr, status, errorThrown ) {
		        alert( "Sorry, there was a problem!" );
		        console.log( "Error: " + errorThrown );
		        console.log( "Status: " + status );
		        console.dir( xhr );
		    },
		 
		    // code to run regardless of success or failure
		    complete: function( xhr, status ) {
		        alert( "The request is complete!" );
		    }
		});
		
		$.getJSON("url", params, function());
		$.post
		$.get
	});

	
</script>