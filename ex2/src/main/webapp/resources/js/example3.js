var myObject = {
    sayHello: function() {
        console.log( "Hi! My name is " + this.myName );
    },
    myName: "Rebecca"
};
 
var secondObject = {
    myName: "Colin"
};
 
myObject.sayHello();                    // "Hi! My name is Rebecca"
myObject.sayHello.call( secondObject ); // "Hi! My name is Colin"


var myName = "the global object";
var sayHello = function() {
    console.log( "Hi! My name is " + this.myName );
};
var myObject = {
    myName: "Rebecca"
};
var myObjectHello = sayHello.bind( myObject );
 
sayHello();      // "Hi! My name is the global object"
myObjectHello(); // "Hi! My name is Rebecca"

var obj = {};
obj.prototype = {}