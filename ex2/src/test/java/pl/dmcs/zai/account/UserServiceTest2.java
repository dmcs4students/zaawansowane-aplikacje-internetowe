package pl.dmcs.zai.account;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class UserServiceTest2 {

	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void addUserTest() {
		ToBeTested service = new ToBeTested();
		
		thrown.expect(IllegalArgumentException.class);
		Assert.assertEquals(0, 1);
		
		service.addUser("ala", "makota");
	}
	
	@BeforeClass
	public static void beforeAllTests() {
		System.out.println("beforeAllTests");
	}
	
	@AfterClass
	public static void afterAllTests() {
		System.out.println("afterAllTests");
	}
	
	@Before
	public void beforeTest() {
		System.out.println("beforeTest");
	}
	
	@After
	public void afterTest() {
		System.out.println("afterTest");
	}
	
	@Test
	public void addUser2Test() {
		ToBeTested service = new ToBeTested();
		
		thrown.expect(IllegalArgumentException.class);
		
		service.addUser("ala", "makota");
	}
	

}

class ToBeTested {

	public void addUser(String login, String password) {
		
		throw new IllegalArgumentException();
	}

}